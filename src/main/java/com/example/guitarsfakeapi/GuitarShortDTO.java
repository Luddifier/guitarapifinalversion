package com.example.guitarsfakeapi;

public class GuitarShortDTO {

    public String getGuitarModel() {
        return guitarModel;
    }

    public void setGuitarModel(String guitarModel) {
        this.guitarModel = guitarModel;
    }

    private String guitarModel;

    public GuitarShortDTO(String guitarModel) {
        this.guitarModel = guitarModel;
    }
}
