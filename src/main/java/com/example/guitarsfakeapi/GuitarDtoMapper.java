package com.example.guitarsfakeapi;

public class GuitarDtoMapper {

    public static GuitarShortDTO mapGuitarShortDto(Guitars guitar) {
        var shortGuitar = new GuitarShortDTO(guitar.getModel().toUpperCase());
        return shortGuitar;

    }


}
